// This code creates an interactive map which constructs a
// polyline based on user clicks. Note that the polyline only appears
// once its path property contains two LatLng coordinates. Polyline is placed
// along walking route. Also provides search box to search location at top
// of map. Points can be deleted by double clicking. Polyline is user movable.

var poly;
var map;
var path = new google.maps.MVCArray();

function initialize() {

    // Make sure the map element exists before continue
    var mapElement = jQuery("#map-canvas");
    if (mapElement.length === 0)
    {
        return;
    }

    var mapOptions = {
        mapTypeId: google.maps.MapTypeId.ROADMAP,
    };
    map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);
    // Center on Sheffield by default
    var defaultBounds = new google.maps.LatLngBounds(
            new google.maps.LatLng(53.3574, -1.4931),
            new google.maps.LatLng(53.4002, -1.4359));
    map.fitBounds(defaultBounds);
    // Create the search box and link it to the UI element.
    var input = (document.getElementById('pac-input'));
    map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);
    var searchBox = new google.maps.places.SearchBox((input));

    // Listen for the event fired when the user selects an item from the
    // pick list. Retrieve the matching places for that item.
    google.maps.event.addListener(searchBox, 'places_changed', function () {
        var places = searchBox.getPlaces();
        if (places.length === 0) {
            return;
        }

        // For each place, get the icon, place name, and location.
        var bounds = new google.maps.LatLngBounds();
        for (var i = 0, place; place = places[i]; i++) {
            bounds.extend(place.geometry.location);
        }

        map.fitBounds(bounds);
    });

    // Bias the SearchBox results towards places that are within the bounds of the
    // current map's viewport.
    google.maps.event.addListener(map, 'bounds_changed', function () {
        var bounds = map.getBounds();
        searchBox.setBounds(bounds);
    });

    var polyOptions = {
        strokeColor: '#000000',
        strokeOpacity: 1.0,
        strokeWeight: 3
    };
    poly = new google.maps.Polyline(polyOptions);

    var routedata = jQuery("textarea#innovage_route_path").val();
    if (routedata)
    {
        var indexOfOpenBracket = routedata.indexOf("(");
        var indexOfLastBracket = routedata.lastIndexOf(')');

        //remove leading and trailing brackets
        routedata = routedata.substring(indexOfOpenBracket + 1, indexOfLastBracket);
        //remove any spaces in path string
        routedata = routedata.replace(/\s+/g, "");

        points = routedata.split("),(");
        locations = [];
        for (var k = 0; k < (points.length); k++) {
            var mData = points[k].split(',');
            var point = new google.maps.LatLng(parseFloat(mData[0].trim()), parseFloat(mData[1].trim()));
            locations.push(point);
            path.push(point);
        }
        poly.setPath(path);
        var bounds = new google.maps.LatLngBounds();
        for (x = 0; x < locations.length; x++)
        {
            bounds.extend(locations[x]);
        }

        google.maps.event.addListener(poly.getPath(), 'insert_at', updatePath);
        google.maps.event.addListener(poly.getPath(), 'remove_at', updatePath);
        google.maps.event.addListener(poly.getPath(), 'set_at', updatePath);

        //TODO - have bug here - if snap to road and too many points then page 
        //crashes. To get around this no longer have snap to road. Also if
        //route has too many points do not allow edit.
        if (locations.length <= 500)
        {
            poly.setEditable(true);
        }
        map.fitBounds(bounds);


    } else
    {
        poly.setEditable(true);
    }

    poly.setMap(map);

    // Add a listener for the click event
    google.maps.event.addListener(map, 'click', addLatLng);
}


// Update the form field innovage_route_path when path is changed
function updatePath()
{
    jQuery("textarea#innovage_route_path").val(path.getArray());
}

/**
 * Handles click events on a map, and adds a new point to the Polyline.
 * @param {google.maps.MouseEvent} event
 */
function addLatLng(event) {

    if (path.getLength() === 0) {
        path.push(event.latLng);
        poly.setPath(path);

        // Add a new marker at the start point on the polyline.
        var marker = new google.maps.Marker({
            position: event.latLng,
            title: '#' + path.getLength(),
            map: map
        });
    } else {
        path.push(event.latLng);

        // Add listner to allow editing of polyline
        google.maps.event.addListener(path, 'insert_at', updatePath);
        google.maps.event.addListener(path, 'remove_at', updatePath);
        google.maps.event.addListener(path, 'set_at', updatePath);

        //updatePath();
    }
}

google.maps.event.addDomListener(window, 'load', initialize);


