=== Innovage Routes ===
Contributors: SinOB
Tags: google maps
Requires at least: WP 4.0.1
Tested up to: WP 4.1
Stable tag: 0.1-alpha
License: GPLv2
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Allows editor and admin users to create routes via google maps.

== Description ==

Use the shortcode [innovage_routes_tool] on a page to allow a user with edit permissions to 
    * Build a route via google maps
    * View/Delete existing routes
    * Edit existing routes

There are currently no admin options.

Designed to be used with innovage_pedometer and innovage_bp_group_challenge

== Installation ==

1. Download
2. Upload to your '/wp-contents/plugins/' directory.
3. Activate the plugin through the 'Plugins' menu in WordPress.
