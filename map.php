<?php

function innovage_routes_view_map($route, $percentage_complete) {
    ?>

    <script type="text/javascript" >
        var map;
        var stepDisplay;
        var marker = null;
        var polyline = null;
        var poly2 = null;
        var infowindow = null;
        var timerHandle = null;

        var steps = [];

        var step = 2000;
        var tick = 100; // milliseconds
        var eol;
        var k = 0;
        var stepnum = 0;
        var lastVertex = 1;

        function createMarker(latlng, label, html) {
            var contentString = '<b>' + label + '</b><br>' + html;
            var marker = new google.maps.Marker({
                position: latlng,
                map: map,
                title: label,
                zIndex: Math.round(latlng.lat() * -100000) << 5
            });
            marker.myname = label;

            google.maps.event.addListener(marker, 'click', function () {
                infowindow.setContent(contentString);
                infowindow.open(map, marker);
            });
            return marker;
        }

        function initialize() {
            infowindow = new google.maps.InfoWindow(
                    {
                        size: new google.maps.Size(100, 50)
                    });

            // Create a map and center it on Sheffield.
            var myOptions = {
                mapTypeId: google.maps.MapTypeId.ROADMAP,
                // disable the default google map ui
                disableDefaultUI: true
            };
            map = new google.maps.Map(document.getElementById("map_canvas_view"), myOptions);

            // Instantiate an info window to hold step text.
            stepDisplay = new google.maps.InfoWindow();

            if (timerHandle) {
                clearTimeout(timerHandle);
            }
            if (marker) {
                // clear marker via the null
                marker.setMap(null);
            }

            polyline = new google.maps.Polyline({
                path: [],
                strokeColor: '#FF0000',
                strokeWeight: 3
            });

            // Remove leading and trailing spaces
            var routedata = jQuery.trim("<?php echo $route->path; ?>");
            var indexOfOpenBracket = routedata.indexOf("(");
            var indexOfLastBracket = routedata.lastIndexOf(')');

            //remove leading and trailing brackets
            routedata = routedata.substring(indexOfOpenBracket+1, indexOfLastBracket);

            points = routedata.split("),(");
            locations = [];
            for (var k = 1; k < (points.length - 1); k++) {
                var mData = points[k].split(',');
                var point = new google.maps.LatLng(parseFloat(mData[0]), parseFloat(mData[1]));
                locations.push(point);
            }

            var bounds = new google.maps.LatLngBounds();

            startLocation = new Object();
            endLocation = new Object();

            // Create the user position marker
            startLocation.latlng = locations[0];
            startLocation.address = 'Your and your partner are here';
            marker = createMarker(startLocation.latlng, startLocation.address, startLocation.address);

            // Create the end marker
            endLocation.latlng = locations[locations.length - 1];
            endLocation.address = 'The end';
            endmarker = createMarker(endLocation.latlng, endLocation.address, endLocation.address);
            endmarker.setIcon('<?php echo plugins_url('race-flag.png', __FILE__); ?>');

            for (x = 1; x < locations.length - 1; x++)
            {
                polyline.getPath().push(locations[x]);
                bounds.extend(locations[x]);
            }

            // Calculate the animation 'step' based on the length of the route
            polyLengthInMeters = google.maps.geometry.spherical.computeLength(polyline.getPath().getArray());
            step = (polyLengthInMeters / 100) * 1;

            polyline.setMap(map);
            map.fitBounds(bounds);
            map.setZoom(12);

            // Once the page loads automatically get the animation going
            startAnimation();
        }


        //=============== animation functions ======================
        // called by animate() function
        function updatePoly(d) {
            // Spawn a new polyline every 20 vertices, because updating a 100-vertex poly is too slow
            if (poly2.getPath().getLength() > 20) {
                poly2 = new google.maps.Polyline({path: [polyline.getPath().getAt(lastVertex - 1)], strokeColor: '#0000FF'});
            }

            if (polyline.GetIndexAtDistance(d) < lastVertex + 2) {
                if (poly2.getPath().getLength() > 1) {
                    poly2.getPath().removeAt(poly2.getPath().getLength() - 1);
                }
                poly2.getPath().insertAt(poly2.getPath().getLength(), polyline.GetPointAtDistance(d));
            } else {
                poly2.getPath().insertAt(poly2.getPath().getLength(), endLocation.latlng);
            }
        }

        // Called by startAnimation after timeout
        function animate(d) {
            if (d >= eol) {
                // Stop with the marker placement
                return;
            }

            var p = polyline.GetPointAtDistance(d);
            if (p)
            {
                marker.setPosition(p);
                updatePoly(d);
                timerHandle = setTimeout("animate(" + (d + step) + ")", tick);
            }
        }

        //called by route calculation function
        function startAnimation() {
            // Set eol to current position by multiplying by percentage of steps walked/steps total
            eol = (polyline.Distance() * <?php echo $percentage_complete; ?> / 100);

            poly2 = new google.maps.Polyline({path: [polyline.getPath().getAt(0)], strokeColor: "#0000FF", strokeWeight: 10});
            setTimeout("animate(50)", 500);  // Allow time for the initial map display
        }
        jQuery(document).ready(function () {
            initialize();
        });

    </script>

    <div id="map_canvas_view" style="width: 500px;height: 500px"></div>
    <?php
}
