<?php
/*
  Plugin Name: Innovage Routes
  Plugin URI: https://bitbucket.org/SinOB/innovage_route_builder
  Description: Allow editor/admin users to create walking routes on top of google maps
  Author: Sinead O'Brien
  Version: 1.2
  Author URI: https://bitbucket.org/SinOB
  Requires at least: WP 4.0.1
  Tested up to: WP 4.1
  License: GNU General Public License 2.0 (GPL) http://www.gnu.org/licenses/gpl.html
 */


global $innovage_routes_db_version;
$innovage_routes_db_version = "1.0";

// Run the install scripts upon plugin activation
register_activation_hook(__FILE__, 'innovage_routes_meta_install');

/** /
 * Installation function. Creates the database table
 * @global type $wpdb
 * @global string $innovage_routes_db_version
 */
function innovage_routes_meta_install() {
    global $wpdb;
    global $innovage_routes_db_version;

    if (get_option("innovage_routes_db_version") != $innovage_routes_db_version) {
        $table_name = $wpdb->prefix . "innovage_routes";

        $sql1 = "Drop table $table_name;";

        $sql2 = "CREATE TABLE $table_name (
        id bigint(20) unsigned NOT NULL AUTO_INCREMENT,
        title text NULL,
        description longtext NULL,
        path LONGBLOB,
        goal_amount INT NOT NULL DEFAULT 0,
        UNIQUE KEY id (id)
        );";

        require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
        dbDelta($sql1);
        dbDelta($sql2);
    }
    update_option("innovage_routes_db_version", $innovage_routes_db_version);
}

/** /
 * Load the page containing map display
 */
function innovage_routes_init() {
    require( dirname(__FILE__) . '/map.php' );
}

add_action('bp_include', 'innovage_routes_init');

// Shortcode for this plugin is [innovage_routes_tool]
add_shortcode('innovage_routes_tool', 'innovage_routes_tool');

/** /
 * Main function controlling what to display for route builder tool
 *
 * @global type $bp
 * @param type $atts
 * @param type $content
 * @return string
 */
function innovage_routes_tool($atts, $content = '') {
    global $bp;

    // If user isn't logged in hide the page
    if (!is_user_logged_in()) {
        return "Please log in to view this content.";
    }

    // Only allow user with edit or admin permission to access route builder
    if (!current_user_can('edit_pages')) {
        return "You do not have sufficient permission to view this content.";
    }

    if ($_SERVER['REQUEST_METHOD'] === 'POST') {
        // Saving or updating a route
        $title = trim(sanitize_text_field($_POST['innovage_route_title']));
        $description = sanitize_text_field($_POST['innovage_route_description']);
        $goal_count = intval($_POST['innovage_route_goal_amount']);
        $path = sanitize_text_field($_POST['innovage_route_path']);
        $id = isset($_POST['innovage_route_id']) ? intval($_POST['innovage_route_id']) : null;

        innovage_routes_save_route($id, $title, $description, $path, $goal_count);
    } else {
        if (isset($_GET['action'])) {

            if ($_GET['action'] == 'add' || $_GET['action'] == 'edit') {
                // Show the route editor
                $route_id = isset($_GET['id']) ? $_GET['id'] : '';
                return innovage_routes_map_form($route_id);
            } else if ($_GET['action'] == 'delete') {
                $route_id = isset($_GET['id']) ? $_GET['id'] : '';
                innovage_routes_delete_route($route_id);
            }
        }
    }
    return innovage_routes_view_routes();
}

/** /
 * Embed the necessary javascript and css scripts on a page conditionally
 * if they have not already been included. Use this rather than traditional
 * wp_enqueue_scripts approach as this allows us to load conditionally based on
 * shortcode.
 * @param type $posts
 * @return type
 */
function innovage_routes_conditionally_add_scripts_and_styles($posts) {

    // Include the scripts required for the plugin
    wp_register_script('googlemaps', 'https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=places,geometry,drawing', false, '3');
    wp_enqueue_script('googlemaps');

    if (bp_is_groups_component()) {
        wp_enqueue_script('v3_epoly', plugins_url('v3_epoly.js', __FILE__));
    }
    if (empty($posts)) {
        return $posts;
    }

    // Check if routes editor shortcode found
    foreach ($posts as $post) {
        if (stripos($post->post_content, '[innovage_routes_tool]') !== false) {
            wp_enqueue_style('route-mapp-css', plugins_url('map.css', __FILE__));
            wp_enqueue_script('innovage-routes-js', plugins_url('map.js', __FILE__));
            break;
        }
    }

    return $posts;
}

add_filter('the_posts', 'innovage_routes_conditionally_add_scripts_and_styles');

/** /
 * Save a new challenge route to the db.
 * @global type $wpdb
 * @param type $id
 * @param type $title
 * @param type $description
 * @param type $path
 * @param type $goal_amount
 * @return type
 */
function innovage_routes_save_route($id, $title, $description, $path, $goal_amount) {
    global $wpdb;
    $table_name = $wpdb->prefix . "innovage_routes";

    if (isset($id) && $id != null) {
        $wpdb->update($table_name, array('title' => $title,
            'description' => $description,
            'path' => $path,
            'goal_amount' => $goal_amount,
                ), array('ID' => $id)
        );
    } else {
        $wpdb->insert($table_name, array('title' => $title,
            'description' => $description,
            'path' => $path,
            'goal_amount' => $goal_amount,
        ));
    }
    return;
}

/** /
 * Delete a route from the db
 * @global type $route_id
 * @return type
 */
function innovage_routes_delete_route($route_id) {
    // Only an admin user is allowed to delete
    if (!current_user_can('manage_options')) {
        return;
    }
    global $wpdb;
    $table_name = $wpdb->prefix . "innovage_routes";

    $wpdb->delete($table_name, array('id' => $route_id));
}

/** /
 * Get all of the routes saved to the database
 * @global type $wpdb
 * @return type
 */
function innovage_routes_get_routes() {
    global $wpdb;
    $table_name = $wpdb->prefix . "innovage_routes";

    $results = $wpdb->get_results("SELECT id, title, description, path, goal_amount"
            . " FROM $table_name ");
    return $results;
}

/** /
 * Get the route based on route_id
 * @global type $wpdb
 * @param type $challenge_id
 * @return type
 */
function innovage_routes_get_route($route_id) {
    global $wpdb;
    $table_name = $wpdb->prefix . "innovage_routes";

    $sql = $wpdb->prepare("SELECT id, title, description, path, goal_amount"
            . " FROM $table_name where id=%s", $route_id);
    $row = $wpdb->get_row($sql);
    return $row;
}

/** /
 * Display a table of the routes with links to edit and delete each route entry.
 */
function innovage_routes_view_routes() {
    $routes = innovage_routes_get_routes();
    $url = get_option('siteurl') . "/route-builder/";
    ?> 
    <div id='innovage_routes_view' >    
        <h3>Routes</h3>
        <a href='<?php echo $url . "?action=add"; ?>'><input type='button' value='Add New Route'></a>
        <table id='blob' class='blob'><tr><th>Title</th><th>Description</th><th>Manage</th></tr>
            <?php foreach ($routes as $route) : ?>
                <tr>
                    <td> <?php echo $route->title ?> </td>
                    <td> <?php echo $route->description ?> </td>
                    <td>
                        <a href='<?php echo $url . "?action=edit&id=" . $route->id; ?>'>Edit</a> 
                        &nbsp;&nbsp;&nbsp; <a href='<?php echo $url . "?action=delete&id=" . $route->id; ?>'>Delete</a>
                    </td>
                </tr>
            <?php endforeach; ?>
        </table>
    </div>        <?php
}

/** /
 * Display route map and route form
 * @param type $route_id
 */
function innovage_routes_map_form($route_id) {
    // Get route info
    if (isset($route_id) && !is_null($route_id)) {
        $route = innovage_routes_get_route($route_id);
    }
    ?> 
    <h3><?php echo isset($route->title) ? 'Edit ' . $route->title : 'Create New Map'; ?></h3>
    <input id="pac-input" class="controls" type="text" placeholder="Search Box">
    <div style="height:600px; width:800px;">
        <div id="map-canvas"></div>
    </div>

    <div id='innovage_route_add' >
        <h3>Create a new route</h3>
        <form id='route-create' method='post'>
            <input type='hidden' name='innovage_route_id' value='<?php echo isset($route->id) ? $route->id : ''; ?>'>
            <label for="innovage_route_title">Route title</label><br/>
            <input type='text' required='required' name='innovage_route_title' value='<?php echo isset($route->title) ? $route->title : ''; ?>'/><br/>
            <label for="innovage_route_description">Route Description</label><br/>
            <textarea rows="4" cols="50" name='innovage_route_description'><?php echo isset($route->description) ? $route->description : ''; ?></textarea><br/>
            <label for="innovage_route_path">Route Path</label><br/>
            <ul>The route path must be in the format (lat1,long1),(lat2,long2),(lat3,long3) e.g. <strong>(53.367,-1.502),(53.367954,-1.503196),(53.36808,-1.503217)</strong>
                Please note:
                <li>* latitude and longitude points are separated by a single comma "," with NO blank spaces.</li>
                <li>* each lat,lng pair is wrapped in round brackets (lat,lng).</li>
                <li>* each (lat,lng) pair is separated from the previous and next pair by a single coma "," with NO blank spaces.</li>
                <li>* the final (lat,lng) pair should have no trailing characters.</li>
            </ul>
            <textarea rows="8" cols="100"  name="innovage_route_path" id='innovage_route_path' type="text" ><?php echo isset($route->path) ? $route->path : ''; ?></textarea><br/>
            <label for="innovage_route_goal_amount">Step count goal</label><br/>
            <input type='text' name='innovage_route_goal_amount' value='<?php echo isset($route->goal_amount) ? $route->goal_amount : ''; ?>'/><br/>
            <input type='hidden' value='add' name='action'>
            <a href='<?php echo $_SERVER['REQUEST_URI'] ?>'><input type='button' name='reset' value='Reset'></a>
            <input type='submit' value='Save'>
        </form>
    </div>
    <?php
}

/** /
 * Helper function to return the challenge step count
 * @param type $route_id
 * @return type
 */
function innovage_routes_get_goal_amount($route_id) {
    $route = innovage_routes_get_route($route_id);
    return $route->goal_amount;
}

/** /
 * Display the non editable route map for the user to see their progress 
 * @param type $route_id
 * @param type $percent_complete
 */
function innovage_routes_get_visualisation($route_id, $percent_complete) {
    $route = $route = innovage_routes_get_route($route_id);
    $challenge_name = $route->title;
    $percent_complete = intval($percent_complete);

    echo "<h3>" . $challenge_name . "</h3>";
    innovage_routes_view_map($route, $percent_complete);
}
